#include <iostream>
#include<conio.h>
using namespace std;
int main()
{
	cout << "Menghitung Keliling Lingkaran" << endl;
	cout << "==========================================" << endl;

	float r, keliling;

	cout << "Masukkan panjang jari-jari lingkaran (cm): ";
	cin >> r;

	keliling = 2 * 3.14 * (r * 2);
	
	cout << "==========================================" << endl;
	cout << "Keliling lingkarannya adalah " << keliling << " cm" << endl;
	getch();
	return 0;
}
